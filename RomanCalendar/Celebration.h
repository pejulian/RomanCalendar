//
//  Celebration.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Celebration : NSObject

@property (assign, nonatomic) NSInteger id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *timestamp;
@property (assign, nonatomic) NSInteger type;
@property (assign, nonatomic) NSInteger calendar;
@property (assign, nonatomic) NSInteger color;
@property (assign, nonatomic) NSInteger season;
@property (assign, nonatomic) NSInteger liturgicalCycle;
@property (assign, nonatomic) NSInteger psalterWeek;

@end
