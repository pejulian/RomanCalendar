//
//  Utilities.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "Utilities.h"

static NSMutableArray *liturgicalCycles = nil;
static NSMutableArray *psalterWeeks = nil;
static NSMutableArray *calendars = nil;
static NSMutableArray *types = nil;
static NSMutableArray *seasons = nil;
static NSMutableArray *colors = nil;

@implementation Utilities

// /Users/pejulian/Library/Developer/CoreSimulator/Devices/86144D9E-8CE8-4877-A6AA-942F38AEB1C4/data/Containers/Data/Application/53927D2C-3449-4182-9491-9B2D2CE79096/Documents/romcal.sqlite

+ (BOOL) addCelebration:(Celebration *)celebration {
    
    NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];

    BOOL success = [database executeUpdate:@"INSERT INTO ROMCAL_CELEBRATIONS (name, description, timestamp, type, calendar, color, season, liturgicalCycle, psalterWeek) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", [celebration name], [celebration description], [celebration timestamp], [celebration type], [celebration calendar], [celebration color], [celebration season], [celebration liturgicalCycle], [celebration psalterWeek], nil];

    [database close];
    
    return success;
}

+ (NSMutableArray *) getCelebrations {
    
    NSMutableArray *items = [NSMutableArray new];
    NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
        // SELECT celeb.id AS celebrationId, celeb.name AS celebrationName, celeb.description AS celebrationDescription, celeb.timestamp, type.id AS typeId, type.name AS typeName, type.description as typeDescription, type.rank, cal.id AS calendarId, cal.name AS calendarName, cal.description AS calendarDescription, cal.locale, colors.id AS colorId, colors.name as colorName, colors.description as colorDescription, colors.hex, seasons.id AS seasonId, seasons.name AS seasonName, seasons.description AS seasonDescription, cycles.id AS cycleId, cycles.name AS cycleName, cycles.description AS cycleDescription, psalter.id AS psalterId, psalter.name AS psalterName, psalter.description AS psalterDescription FROM ROMCAL_CELEBRATIONS AS celeb, ROMCAL_TYPES AS type, ROMCAL_CALENDARS AS cal, ROMCAL_COLORS AS colors, ROMCAL_SEASONS as seasons, ROMCAL_LITURGICAL_CYCLES as cycles, ROMCAL_PSALTER_WEEKS as psalter JOIN ROMCAL_TYPES ON celeb.type = ROMCAL_TYPES.id  JOIN ROMCAL_CALENDARS ON celeb.calendar = ROMCAL_CALENDARS.id JOIN ROMCAL_COLORS ON celeb.color = ROMCAL_COLORS.id JOIN ROMCAL_SEASONS ON celeb.season = ROMCAL_SEASONS.id JOIN ROMCAL_LITURGICAL_CYCLES on celeb.liturgicalCycle = ROMCAL_LITURGICAL_CYCLES.id JOIN ROMCAL_PSALTER_WEEKS on celeb.psalterWeek = ROMCAL_PSALTER_WEEKS.id
    
    FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_CELEBRATIONS"];
    while ( [results next] ) {
        
        Celebration *item = [Celebration new];
        [item setId:[results intForColumn:@"id"]];
        [item setName:[results stringForColumn:@"name"]];
        [item setDescription:[results stringForColumn:@"description"]];
        [item setTimestamp:[results dateForColumn:@"timestamp"]];
        [item setType:[results intForColumn:@"type"]];
        [item setCalendar:[results intForColumn:@"calendar"]];
        [item setColor:[results intForColumn:@"color"]];
        [item setSeason:[results intForColumn:@"season"]];
        [item setLiturgicalCycle:[results intForColumn:@"liturgicalCycle"]];
        [item setPsalterWeek:[results intForColumn:@"psalterWeek"]];
        
        [items addObject:item];
    }
    
    [database close];
    
    return items;
}

+ (NSMutableArray *) getLiturgicalCycles {
    
    if ( liturgicalCycles == nil ) {
        
        liturgicalCycles = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_LITURGICAL_CYCLES"];
        while ( [results next] ) {
            
            LiturgicalCycle *item = [LiturgicalCycle new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            
            [liturgicalCycles addObject:item];
        }
        
        [database close];
        
    }

    return liturgicalCycles;
}

+ (NSMutableArray *) getPsalterWeeks {
    
    if ( psalterWeeks == nil ) {
        
        psalterWeeks = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_PSALTER_WEEKS"];
        while ( [results next] ) {
            
            PsalterWeek *item = [PsalterWeek new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            
            [psalterWeeks addObject:item];
        }
        
        [database close];
    }

    return psalterWeeks;
}

+ (NSMutableArray *) getCalendars {
    
    if ( calendars == nil ) {
        
        calendars = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_CALENDARS"];
        while ( [results next] ) {
            
            Calendar *item = [Calendar new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            [item setLocale:[results stringForColumn:@"locale"]];
            
            [calendars addObject:item];
        }
        
        [database close];
    }
    
    return calendars;
}

+ (NSMutableArray *) getColors {
    
    if ( colors == nil ) {
        
        colors = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_COLORS"];
        while ( [results next] ) {
            
            Color *item = [Color new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            [item setHex:[results stringForColumn:@"hex"]];
            
            [colors addObject:item];
        }
        
        [database close];
    }
    
    return colors;
}

+ (NSMutableArray *) getSeasons {
    
    if ( seasons == nil ) {
        
        seasons = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_SEASONS"];
        while ( [results next] ) {
            
            Season *item = [Season new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            
            [seasons addObject:item];
        }
        
        [database close];
    }

    return seasons;
}

+ (NSMutableArray *) getTypes {
    
    if ( types == nil ) {
        
        types = [NSMutableArray new];
        NSString *databasePath = [[NSUserDefaults standardUserDefaults] valueForKey:@"databasePath"];
        FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
        [database open];
        
        FMResultSet *results = [database executeQuery:@"SELECT * FROM ROMCAL_TYPES"];
        while ( [results next] ) {
            
            Type *item = [Type new];
            [item setId:[results intForColumn:@"id"]];
            [item setName:[results stringForColumn:@"name"]];
            [item setDescription:[results stringForColumn:@"description"]];
            [item setRank:[results intForColumn:@"rank"]];
            [types addObject:item];
        }
        
        [database close];
    }

    return types;
}


+ (YLMoment*) toMoment:(NSDate*) date {
    return [YLMoment momentWithDate:date];
}

+ (YLMoment*) toMomentFromDateString:(NSString*) date {
    YLMoment *moment = [YLMoment momentWithDateAsString:date format:@"yyyy-MM-dd'T'HH:mm:ss'Z'" locale:[NSLocale currentLocale] timeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    return moment;
}

+ (NSString *) prettyTime:(YLMoment*)moment {
    return [moment format:@"h:mm a"];
}

+ (NSString *) prettyDate:(YLMoment*)moment {
    return [moment format:@"MM/dd/yyyy"];
}

+ (NSString *) prettyDateAndTime:(YLMoment*)moment {
    return [moment format:@"EEE, MMM dd yyyy, h:mm a"];
}

+ (NSString*) ISO8601DateString:(NSDate*)date {
    YLMoment *moment = [Utilities toMoment:date];
    moment.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    return [moment format:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
}

@end
