//
//  RCTextView.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "RCTextView.h"

@implementation RCTextView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 5 );
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 5 );
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [self setContentInset:UIEdgeInsetsMake(3, 3, 3, 3)];

    [self setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
    
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor=[[UIColor lightTextColor] CGColor];
    self.layer.cornerRadius = 0;
}

@end
