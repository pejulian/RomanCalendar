//
//  NewCelebrationViewController.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "NewCelebrationViewController.h"

@interface NewCelebrationViewController ()

@end

@implementation NewCelebrationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    FAKIcon *submitIcon = [FAKIonIcons ios7PaperplaneIconWithSize:32];
    [submitIcon addAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    FAKIcon *backIcon = [FAKIonIcons ios7ArrowLeftIconWithSize:32];
    [backIcon addAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];

    UIBarButtonItem *addRecord = [[UIBarButtonItem alloc] initWithImage:[submitIcon imageWithSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(addRecord:)];
    self.navigationItem.rightBarButtonItem = addRecord;

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[backIcon imageWithSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(backButton:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [self.navigationItem setTitle:@"New Celebration"];

    self.types = [Utilities getTypes];
    self.colors = [Utilities getColors];
    self.seasons = [Utilities getSeasons];
    self.calendars = [Utilities getCalendars];
    self.liturgicalCycles = [Utilities getLiturgicalCycles];
    self.psalterWeeks = [Utilities getPsalterWeeks];

    self.typePicker = [[UIPickerView alloc] init];
    [self.typePicker setTag:0];
    [self.typePicker setDelegate:self];
    [self.typePicker setDataSource:self];
    [self.typePicker setShowsSelectionIndicator:YES];
    [self.celebrationType setInputView:self.typePicker];
    
    self.colorPicker = [[UIPickerView alloc] init];
    [self.colorPicker setTag:1];
    [self.colorPicker setDelegate:self];
    [self.colorPicker setDataSource:self];
    [self.colorPicker setShowsSelectionIndicator:YES];
    [self.celebrationColor setInputView:self.colorPicker];
    
    self.seasonPicker = [[UIPickerView alloc] init];
    [self.seasonPicker setTag:2];
    [self.seasonPicker setDelegate:self];
    [self.seasonPicker setDataSource:self];
    [self.seasonPicker setShowsSelectionIndicator:YES];
    [self.celebrationSeason setInputView:self.seasonPicker];
    
    self.calendarPicker = [[UIPickerView alloc] init];
    [self.calendarPicker setTag:3];
    [self.calendarPicker setDelegate:self];
    [self.calendarPicker setDataSource:self];
    [self.calendarPicker setShowsSelectionIndicator:YES];
    [self.celebrationCalendar setInputView:self.calendarPicker];
    
    self.liturgicalCyclePicker = [[UIPickerView alloc] init];
    [self.liturgicalCyclePicker setTag:4];
    [self.liturgicalCyclePicker setDelegate:self];
    [self.liturgicalCyclePicker setDataSource:self];
    [self.liturgicalCyclePicker setShowsSelectionIndicator:YES];
    [self.celebrationLiturgicalCycle setInputView:self.liturgicalCyclePicker];
    
    self.psalterWeekPicker = [[UIPickerView alloc] init];
    [self.psalterWeekPicker setTag:5];
    [self.psalterWeekPicker setDelegate:self];
    [self.psalterWeekPicker setDataSource:self];
    [self.psalterWeekPicker setShowsSelectionIndicator:YES];
    [self.celebrationPsalterWeek setInputView:self.psalterWeekPicker];
    
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setDatePickerMode:UIDatePickerModeDate];
    [self.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.celebrationDate setInputView:self.datePicker];
    
    
    // Setup default values
    [self.calendarPicker selectRow:0 inComponent:0 animated:YES];
    [self.liturgicalCyclePicker selectRow:0 inComponent:0 animated:YES];
    [self.typePicker selectRow:0 inComponent:0 animated:YES];
    [self.psalterWeekPicker selectRow:0 inComponent:0 animated:YES];
    [self.seasonPicker selectRow:0 inComponent:0 animated:YES];
    [self.colorPicker selectRow:0 inComponent:0 animated:YES];
    
    [self.celebrationDate setText:[Utilities prettyDate:[Utilities toMoment:[NSDate date]]]];
    [self.celebrationColor setText:[[self.colors firstObject] name]];
    [self.celebrationLiturgicalCycle setText:[[self.liturgicalCycles firstObject] name]];
    [self.celebrationPsalterWeek setText:[[self.psalterWeeks firstObject] name]];
    [self.celebrationType setText:[[self.types firstObject] name]];
    [self.celebrationSeason setText:[[self.seasons firstObject] name]];
    [self.celebrationCalendar setText:[[self.calendars firstObject] name]];

}


- (void) datePickerValueChanged:(id)sender {
    
    NSDate *date = [self.datePicker date];
    self.celebrationDate.text = [Utilities prettyDate:[Utilities toMoment:date]];
}

- (void) textFieldDidEndEditing:(UITextField *)textField {
    
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    NSInteger rows = 0;
    
    switch ( [pickerView tag] ) {
        case 0:
            rows = [self.types count];
            break;
        case 1:
            rows = [self.colors count];
            break;
        case 2:
            rows = [self.seasons count];
            break;
        case 3:
            rows = [self.calendars count];
            break;
        case 4:
            rows = [self.liturgicalCycles count];
            break;
        case 5:
            rows = [self.psalterWeeks count];
            break;
        default:
            break;
    }
    
    return rows;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = @"";
    
    switch ( [pickerView tag] ) {
        case 0:
        {
            Type *type = [self.types objectAtIndex:row];
            title = [type name];
        }
            break;
        case 1:
        {
            Color *color = [self.colors objectAtIndex:row];
            title = [color name];
        }
            break;
        case 2:
        {
            Season *season = [self.seasons objectAtIndex:row];
            title = [season name];
        }
            break;
        case 3:
        {
            Calendar *calendar = [self.calendars objectAtIndex:row];
            title = [calendar name];
        }
            break;
        case 4:
        {
            LiturgicalCycle *liturgicalCycle = [self.liturgicalCycles objectAtIndex:row];
            title = [liturgicalCycle name];
        }
            break;
        case 5:
        {
            PsalterWeek *psalterWeek = [self.psalterWeeks objectAtIndex:row];
            title = [psalterWeek name];
        }
            break;
        default:
            break;
    }
    
    return title;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch ( [pickerView tag] ) {
            
        case 0:
        {
            Type *type = [self.types objectAtIndex:row];
            self.celebrationType.text = [type name];
        }
            break;
        case 1:
        {
            Color *color = [self.colors objectAtIndex:row];
            self.celebrationColor.text = [color name];
        }
            break;
        case 2:
        {
            Season *season = [self.seasons objectAtIndex:row];
            self.celebrationSeason.text = [season name];
        }
            break;
        case 3:
        {
            Calendar *calendar = [self.calendars objectAtIndex:row];
            self.celebrationCalendar.text = [calendar name];
        }
            break;
        case 4:
        {
            LiturgicalCycle *liturgicalCycle = [self.liturgicalCycles objectAtIndex:row];
            self.celebrationLiturgicalCycle.text = [liturgicalCycle name];
        }
            break;
        case 5:
        {
            PsalterWeek *psalterWeek = [self.psalterWeeks objectAtIndex:row];
            self.celebrationPsalterWeek.text = [psalterWeek name];
        }
            break;
        default:
            break;
    }
}


- (void) addRecord:(id)sender {
    
    Celebration *celebration = [Celebration new];

    [celebration setName:self.celebrationName.text];
    [celebration setDescription:self.celebrationDescription.text];
    [celebration setSeason:[[self.seasons objectAtIndex:[self.seasonPicker selectedRowInComponent:0]] id]];
    [celebration setPsalterWeek:[[self.psalterWeeks objectAtIndex:[self.psalterWeekPicker selectedRowInComponent:0]] id]];
    [celebration setLiturgicalCycle:[[self.liturgicalCycles objectAtIndex:[self.liturgicalCyclePicker selectedRowInComponent:0]] id]];
    [celebration setColor:[[self.colors objectAtIndex:[self.colorPicker selectedRowInComponent:0]] id]];
    [celebration setCalendar:[[self.calendars objectAtIndex:[self.calendarPicker selectedRowInComponent:0]] id]];
    [celebration setType:[[self.types objectAtIndex:[self.typePicker selectedRowInComponent:0]] id]];
    
    NSDate *date = [self.datePicker date];
    [celebration setTimestamp:[Utilities ISO8601DateString:date]];
    
    BOOL result = [Utilities addCelebration:celebration];
}

- (void) backButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
