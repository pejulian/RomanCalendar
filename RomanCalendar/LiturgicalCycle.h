//
//  LiturgicalCycle.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiturgicalCycle : NSObject

@property (assign, nonatomic) NSInteger id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *description;

@end
