//
//  RCTextField.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "RCTextField.h"

@implementation RCTextField

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 0 );
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 5 , 0 );
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [self setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16]];
    
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor=[[UIColor lightTextColor] CGColor];
    
}


@end
