//
//  CelebrationsTableViewController.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "CelebrationsTableViewController.h"

@interface CelebrationsTableViewController ()

@end

@implementation CelebrationsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    
    FAKIcon *icon = [FAKIonIcons ios7PlusEmptyIconWithSize:32];
    [icon addAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    UIBarButtonItem *addCelebration = [[UIBarButtonItem alloc] initWithImage:[icon imageWithSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(addCelebration:)];
    self.navigationItem.rightBarButtonItem = addCelebration;
    
    
    self.seasons = [Utilities getSeasons];
    self.celebrations = [Utilities getCelebrations];
    
    [self.tableView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addCelebration:(id)sender {
    [self performSegueWithIdentifier:@"addCelebrationSegue" sender:sender];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.celebrations count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];

    if ( cell == nil )
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    
    Celebration *celebration = [self.celebrations objectAtIndex:[indexPath row]];
    
    if ( celebration != nil ) {
        
        [cell.textLabel setText:[celebration name]];
        
        __block Season *season = nil;
        [self.seasons each:^(Season *s) {
            if ( [s id] == [celebration season] )
                season = s;
        }];

        [cell.detailTextLabel setText:[season name]];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Celebration *celebration = [self.celebrations objectAtIndex:[indexPath row]];
    [self performSegueWithIdentifier:@"editCelebrationSegue" sender:celebration];
    
}

#pragma mark - Segue management

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    
    if( [[segue identifier] isEqualToString:@"addCelebrationSegue"]) {
        
    }
    else if( [[segue identifier] isEqualToString:@"editCelebrationSegue"] ) {
        
        Celebration *celebration = (Celebration *)sender;
        [[segue destinationViewController] setCelebration:celebration];
        
    }
    else {}
}


@end
