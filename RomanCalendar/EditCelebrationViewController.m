//
//  EditCelebrationViewController.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "EditCelebrationViewController.h"

@interface EditCelebrationViewController ()

@end

@implementation EditCelebrationViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    FAKIcon *submitIcon = [FAKIonIcons ios7PaperplaneIconWithSize:32];
    [submitIcon addAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    FAKIcon *backIcon = [FAKIonIcons ios7ArrowLeftIconWithSize:32];
    [backIcon addAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]}];
    
    UIBarButtonItem *editRecord = [[UIBarButtonItem alloc] initWithImage:[submitIcon imageWithSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(editRecord:)];
    self.navigationItem.rightBarButtonItem = editRecord;
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[backIcon imageWithSize:CGSizeMake(32, 32)] style:UIBarButtonItemStylePlain target:self action:@selector(backButton:)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationItem.title = @"Edit Celebration";

    self.types = [Utilities getTypes];
    self.colors = [Utilities getColors];
    self.seasons = [Utilities getSeasons];
    self.calendars = [Utilities getCalendars];
    self.liturgicalCycles = [Utilities getLiturgicalCycles];
    self.psalterWeeks = [Utilities getPsalterWeeks];
    
    self.typePicker = [[UIPickerView alloc] init];
    [self.typePicker setTag:0];
    [self.typePicker setDelegate:self];
    [self.typePicker setDataSource:self];
    [self.typePicker setShowsSelectionIndicator:YES];
    [self.celebrationType setInputView:self.typePicker];
    
    self.colorPicker = [[UIPickerView alloc] init];
    [self.colorPicker setTag:1];
    [self.colorPicker setDelegate:self];
    [self.colorPicker setDataSource:self];
    [self.colorPicker setShowsSelectionIndicator:YES];
    [self.celebrationColor setInputView:self.colorPicker];
    
    self.seasonPicker = [[UIPickerView alloc] init];
    [self.seasonPicker setTag:2];
    [self.seasonPicker setDelegate:self];
    [self.seasonPicker setDataSource:self];
    [self.seasonPicker setShowsSelectionIndicator:YES];
    [self.celebrationSeason setInputView:self.seasonPicker];
    
    self.calendarPicker = [[UIPickerView alloc] init];
    [self.calendarPicker setTag:3];
    [self.calendarPicker setDelegate:self];
    [self.calendarPicker setDataSource:self];
    [self.calendarPicker setShowsSelectionIndicator:YES];
    [self.calendarPicker selectRow:0 inComponent:0 animated:YES];
    [self.celebrationCalendar setInputView:self.calendarPicker];
    
    self.liturgicalCyclePicker = [[UIPickerView alloc] init];
    [self.liturgicalCyclePicker setTag:4];
    [self.liturgicalCyclePicker setDelegate:self];
    [self.liturgicalCyclePicker setDataSource:self];
    [self.liturgicalCyclePicker setShowsSelectionIndicator:YES];
    [self.celebrationLiturgicalCycle setInputView:self.liturgicalCyclePicker];
    
    self.psalterWeekPicker = [[UIPickerView alloc] init];
    [self.psalterWeekPicker setTag:5];
    [self.psalterWeekPicker setDelegate:self];
    [self.psalterWeekPicker setDataSource:self];
    [self.psalterWeekPicker setShowsSelectionIndicator:YES];
    [self.celebrationPsalterWeek setInputView:self.psalterWeekPicker];
    
    self.datePicker = [[UIDatePicker alloc] init];
    [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    [self.datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.celebrationDate setInputView:self.datePicker];
    
    [self configureView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setCelebration:(Celebration *)celebration {
    if ( _celebration != celebration )
        _celebration = celebration;
    [self configureView];
}

- (void) datePickerValueChanged:(id)sender {
    
    NSDate *date = [self.datePicker date];
    self.celebrationDate.text = [Utilities prettyDateAndTime:[Utilities toMoment:date]];
}

- (void) configureView {

    __block Color *color = nil;
    [self.colors each:^(Color *c) {
        if ( c.id == [self.celebration color] )
            color = c;
    }];
    
    __block Season *season = nil;
    [self.seasons each:^(Season *s) {
        if ( s.id == [self.celebration season] )
            season = s;
    }];
    
    __block Type *type = nil;
    [self.types each:^(Type *t) {
        if ( t.id == [self.celebration type] )
            type = t;
    }];
    
    __block LiturgicalCycle *liturgicalCycle = nil;
    [self.liturgicalCycles each:^(LiturgicalCycle *lc) {
        if ( lc.id == [self.celebration liturgicalCycle] )
            liturgicalCycle = lc;
    }];
    
    __block Calendar *calendar = nil;
    [self.calendars each:^(Calendar *c) {
        if ( c.id == [self.celebration calendar] )
            calendar = c;
    }];
    
    __block PsalterWeek *psalterWeek = nil;
    [self.types each:^(PsalterWeek *pw) {
        if ( pw.id == [self.celebration psalterWeek] )
            psalterWeek = pw;
    }];
    
    self.celebrationName.text = [self.celebration name];
    self.celebrationDescription.text = [self.celebration description];
    self.celebrationColor.text = [color name];
    self.celebrationCalendar.text = [calendar name];
    self.celebrationPsalterWeek.text = [psalterWeek name];
    self.celebrationLiturgicalCycle.text = [liturgicalCycle name];
    self.celebrationSeason.text = [season name];
    self.celebrationType.text = [type name];
    self.celebrationDate.text = [Utilities prettyDateAndTime:[Utilities toMomentFromDateString:[self.celebration timestamp]]];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    NSInteger rows = 0;
    
    switch ( [pickerView tag] ) {
        case 0:
            rows = [self.types count];
            break;
        case 1:
            rows = [self.colors count];
            break;
        case 2:
            rows = [self.seasons count];
            break;
        case 3:
            rows = [self.calendars count];
            break;
        case 4:
            rows = [self.liturgicalCycles count];
            break;
        case 5:
            rows = [self.psalterWeeks count];
            break;
        default:
            break;
    }
    
    return rows;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = @"";
    
    switch ( [pickerView tag] ) {
        case 0:
        {
            Type *type = [self.types objectAtIndex:row];
            title = [type name];
        }
            break;
        case 1:
        {
            Color *color = [self.colors objectAtIndex:row];
            title = [color name];
        }
            break;
        case 2:
        {
            Season *season = [self.seasons objectAtIndex:row];
            title = [season name];
        }
            break;
        case 3:
        {
            Calendar *calendar = [self.calendars objectAtIndex:row];
            title = [calendar name];
        }
            break;
        case 4:
        {
            LiturgicalCycle *liturgicalCycle = [self.liturgicalCycles objectAtIndex:row];
            title = [liturgicalCycle name];
        }
            break;
        case 5:
        {
            PsalterWeek *psalterWeek = [self.psalterWeeks objectAtIndex:row];
            title = [psalterWeek name];
        }
            break;
        default:
            break;
    }
    
    return title;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    switch ( [pickerView tag] ) {

        case 0:
        {
            Type *type = [self.types objectAtIndex:row];
            self.celebrationType.text = [type name];
        }
            break;
        case 1:
        {
            Color *color = [self.colors objectAtIndex:row];
            self.celebrationColor.text = [color name];
        }
            break;
        case 2:
        {
            Season *season = [self.seasons objectAtIndex:row];
            self.celebrationSeason.text = [season name];
        }
            break;
        case 3:
        {
            Calendar *calendar = [self.calendars objectAtIndex:row];
            self.celebrationCalendar.text = [calendar name];
        }
            break;
        case 4:
        {
            LiturgicalCycle *liturgicalCycle = [self.liturgicalCycles objectAtIndex:row];
            self.celebrationLiturgicalCycle.text = [liturgicalCycle name];
        }
            break;
        case 5:
        {
            PsalterWeek *psalterWeek = [self.psalterWeeks objectAtIndex:row];
            self.celebrationPsalterWeek.text = [psalterWeek name];
        }
            break;
        default:
            break;
    }
}


- (void) editRecord:(id)sender {
    
    
    
    
}

- (void) backButton:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
