//
//  RCViewController.m
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/27/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "RCViewController.h"

@interface RCViewController ()

@end

@implementation RCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [self findAllTextFieldsInView:self.view];
}

-(void)findAllTextFieldsInView:(UIView*)view {
    
    NSInteger tag = 0;

    for( id x in [view subviews] ) {
        
        if( [x isKindOfClass:[UITextField class]] ) {
            UITextField *textField = (UITextField *)x;
            [textField setDelegate:self];
            [textField setTag:tag];
            tag = tag + 1;
        }
        
        // Recursively apply to nested views too
        if( [x respondsToSelector:@selector(subviews)] ) {
            [self findAllTextFieldsInView:x];
        }
    }
}

-(void)dismissKeyboard {
    
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    NSInteger nextTag = textField.tag + 1;
    UIResponder *nextReponder = [self.view viewWithTag:nextTag];
    
    if ( nextReponder )
        [nextReponder becomeFirstResponder];
    else
        [textField resignFirstResponder];
    
    return YES;
}

@end
