//
//  RCTextView.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCTextView : UITextView

@end
