//
//  EditCelebrationViewController.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/28/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import "RCViewController.h"

#import <FontAwesomeKit/FontAwesomeKit.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import "Utilities.h"

#import "Type.h"
#import "Color.h"
#import "LiturgicalCycle.h"
#import "PsalterWeek.h"
#import "Celebration.h"
#import "Season.h"
#import "Calendar.h"

@interface EditCelebrationViewController : RCViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (strong, nonatomic) Celebration *celebration;

@property (strong, nonatomic) NSMutableArray *types;
@property (strong, nonatomic) NSMutableArray *calendars;
@property (strong, nonatomic) NSMutableArray *colors;
@property (strong, nonatomic) NSMutableArray *seasons;
@property (strong, nonatomic) NSMutableArray *liturgicalCycles;
@property (strong, nonatomic) NSMutableArray *psalterWeeks;

@property (strong, nonatomic) UIPickerView *typePicker;
@property (strong, nonatomic) UIPickerView *colorPicker;
@property (strong, nonatomic) UIPickerView *seasonPicker;
@property (strong, nonatomic) UIPickerView *calendarPicker;
@property (strong, nonatomic) UIPickerView *liturgicalCyclePicker;
@property (strong, nonatomic) UIPickerView *psalterWeekPicker;
@property (strong, nonatomic) UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UITextField *celebrationName;
@property (weak, nonatomic) IBOutlet UITextField *celebrationType;
@property (weak, nonatomic) IBOutlet UITextField *celebrationSeason;
@property (weak, nonatomic) IBOutlet UITextField *celebrationCalendar;
@property (weak, nonatomic) IBOutlet UITextField *celebrationColor;
@property (weak, nonatomic) IBOutlet UITextField *celebrationPsalterWeek;
@property (weak, nonatomic) IBOutlet UITextField *celebrationLiturgicalCycle;
@property (weak, nonatomic) IBOutlet UITextField *celebrationDate;
@property (weak, nonatomic) IBOutlet UITextView *celebrationDescription;

@end
