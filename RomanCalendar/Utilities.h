//
//  Utilities.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LiturgicalCycle.h"
#import "PsalterWeek.h"
#import "Celebration.h"
#import "Calendar.h"
#import "Color.h"
#import "Season.h"
#import "Type.h"

#import <ObjectiveSugar/ObjectiveSugar.h>
#import <YLMoment/YLMoment.h>

#import "sqlite3.h"
#import "FMDatabase.h"

@interface Utilities : NSObject

+ (BOOL) addCelebration:(Celebration *)celebration;

+ (NSMutableArray *) getCelebrations;
+ (NSMutableArray *) getLiturgicalCycles;
+ (NSMutableArray *) getPsalterWeeks;
+ (NSMutableArray *) getCalendars;
+ (NSMutableArray *) getColors;
+ (NSMutableArray *) getSeasons;
+ (NSMutableArray *) getTypes;

+ (YLMoment*) toMoment:(NSDate*) date;
+ (YLMoment*) toMomentFromDateString:(NSString*) date;
+ (NSString*) prettyTime:(YLMoment*)moment;
+ (NSString*) prettyDate:(YLMoment*)moment;
+ (NSString*) prettyDateAndTime:(YLMoment*)moment;
+ (NSString*) ISO8601DateString:(NSDate*)date;

@end
