//
//  CelebrationsTableViewController.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/26/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FontAwesomeKit/FontAwesomeKit.h>
#import <ObjectiveSugar/ObjectiveSugar.h>

#import "Utilities.h"

#import "Season.h"
#import "Celebration.h"

#import "EditCelebrationViewController.h"

@interface CelebrationsTableViewController : UITableViewController

@property (strong, nonatomic) NSMutableArray *seasons;
@property (strong, nonatomic) NSMutableArray *celebrations;

@end
