//
//  RCViewController.h
//  RomanCalendar
//
//  Created by Julian Matthew Nunis Pereira on 10/27/14.
//  Copyright (c) 2014 Julian Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate>

@end
